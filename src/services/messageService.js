import { v4 } from 'uuid';
import { callApi } from '../helpers/apiHelper';
import { messagesURL } from '../config';


class MessageService {
    getNewId = () => {
        return v4();
    }
    
    getAllMessages = async () => {
        const messages = await callApi(messagesURL);
        
        const sortedMessagesList = messages.slice().sort((a, b) => {
            return a.createdAt < b.createdAt ? 1 : -1;
        })
        console.log(sortedMessagesList)

        return sortedMessagesList;
    }
}
export const messageService = new MessageService();