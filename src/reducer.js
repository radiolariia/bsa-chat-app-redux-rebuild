import { combineReducers } from 'redux';

import { chatReducer } from './components/Chat/reducer';

export const rootReducer = combineReducers({
  chatReducer
});
