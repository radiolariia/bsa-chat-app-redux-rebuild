import React from 'react';
import './LoadingScreen.css';

function LoadingScreen() {
    return (
        <div id="loader-container">
            <div className="loader"></div>
        </div>
    );
}

export default LoadingScreen;