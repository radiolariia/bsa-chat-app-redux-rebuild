import React from 'react';
import './MessageList.css';
import Message from '../Message/Message';
import DayLabel from '../DayLabel/DayLabel';

function MessageList({
  messages, 
  activeUser, 
  onDeleteMessage, 
  showModal,
  hideModal,
  onEditMessage,
  isModalOpen
}) {
  let messageCreationDate = new Date(messages[0].createdAt);
  return (
      <div id="messages-feed">
        <DayLabel createdAt={messages[0].createdAt} />
        {
        messages.map((message, index) => {
          const currMessageCreationDate = new Date(message.createdAt);
          return (
            // add zeros to form a unique identifier
          <React.Fragment key={'000000' + index}>
            {currMessageCreationDate > messageCreationDate ? 
            <DayLabel createdAt={message.createdAt} key={index}/>
            : null}
            <Message 
              message={message} 
              activeUser={activeUser} 
              key={'0000' + index}
              onDeleteMessage={onDeleteMessage}
              showModal={showModal}
              hideModal={hideModal}
              onEditMessage={onEditMessage}
              isModalOpen={isModalOpen}
            />
          </React.Fragment>
          )
        })
        }
      </div>
  );
}

export default MessageList;