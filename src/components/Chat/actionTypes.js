export const SEND_MESSAGE = 'MESSAGE_ACTION:SEND_MESSAGE';
export const SET_ALL_MESSAGES = 'MESSAGE_ACTION:SET_ALL_MESSAGES';
export const TOGGLE_LIKE = 'MESSAGE_ACTION:TOGGLE_LIKE';
export const TOGGLE_LOADING = 'MESSAGE_ACTION:TOGGLE_LOADING';
export const DELETE_MESSAGE = 'MESSAGE_ACTION:DELETE_MESSAGE';
export const EDIT_MESSAGE = 'MESSAGE_ACTION:EDIT_MESSAGE';
export const CANCEL_EDIT_MESSAGE = 'MESSAGE_ACTION:CANCEL_EDIT_MESSAGE';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';