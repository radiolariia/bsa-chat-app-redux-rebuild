import {
    SEND_MESSAGE, 
    SET_ALL_MESSAGES,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    CANCEL_EDIT_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL
} from './actionTypes';
import { messageService } from '../../services/messageService';

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        id: messageService.getNewId(), 
        message
    }
});

// bug with delete action is caused by incorrect API data 
//- all messages from a particular user have the same id what is usually prohibited
export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const setMessages = () => async (dispatch) => {
    const messages = await messageService.getAllMessages();
    console.log(messages)
    dispatch({
        type: SET_ALL_MESSAGES,
        payload: {
            messages,
            isLoading: false
        }
    });
};

export const editMessage = (id, message) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        message
    }
});

export const cancelEditMessage = () => ({
    type: CANCEL_EDIT_MESSAGE
});

export const showModal = () => ({
    type: SHOW_MODAL
});

export const hideModal = () => ({
    type: HIDE_MODAL
});

