import {
    SEND_MESSAGE, 
    SET_ALL_MESSAGES,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    CANCEL_EDIT_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL
} from './actionTypes';

const initialState = {
    isLoading: true,
    messages: [],
    isModalOpen: false,
    editingMessageText: '',
}

export const chatReducer = (state = initialState, action) => {
    switch(action.type) {
        case SEND_MESSAGE:
            const { id, message } = action.payload;
            const newMessage = { id, ...message };

            return {
                ...state,
                messages: [...(state.messages || []), newMessage],
            }
        case SET_ALL_MESSAGES:
            const { messages, isLoading } = action.payload;
            
            return {
                ...state,
                isLoading,
                messages: [...(state.messages || []), ...messages]
            }

        // bug with delete action is caused by incorrect API data 
        //- all messages from a particular user have the same id what is usually prohibited
        case DELETE_MESSAGE:
            const filteredMessages = state.messages.slice().filter(msg => msg.id !== action.payload.id);

            return {
                ...state,
                messages: [...filteredMessages],
            }
        case EDIT_MESSAGE:
            const updatedMessages = state.messages.map(msg => {
                if(msg.id === action.payload.id) {
                    return {
                        ...msg,
                        ...action.payload.message
                    }
                } else {
                    return msg
                }
            })
            return {
                ...state,
                messages: updatedMessages,
            }
        case CANCEL_EDIT_MESSAGE:
            return {
                ...state,
                editingMessageText: ''
            }
        case SHOW_MODAL:
            return {
                ...state,
                isModalOpen: true,
            }
        case HIDE_MODAL:
            return {
                ...state,
                isModalOpen: false,
            }
        default:
            return state;
    }
}